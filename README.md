# Description
This is an extension for Firefox which adds a chart visualizing an order book available at [gragieldowa.pl](https://gragieldowa.pl/spolka_arkusz_zl).

# Before and After
<img src="screenshots/before.png" width="500" height="500" />
<img src="screenshots/after.png" width="500" height="500" />
