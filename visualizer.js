let host = document.getElementsByClassName("profile")[0];
let container = document.createElement("div");
container.classList.add("order-book-visualizer-container");
host.prepend(container);

container.innerHTML =
  "<canvas id='orderBookChart' width=1920 height=1></canvas>" +
  "<div class='order-book-visualizer-input-container'>" +
  "<label for='minInput'>From:</label>" +
  "<input id='minInput' type='number' value=-1>" +
  "<label for='maxInput'>To:</label>" +
  "<input id='maxInput' type='number' value=-1>" +
  "<button id='accumulatedButton' class='order-book-visualizer-button'>Accumulated</button>" +
  "<button id='ordersButton' class='order-book-visualizer-button'>Orders</button>" +
  "<button id='accumulatedAndOrdersButton' class='order-book-visualizer-button'>Accumulated + Orders</button>"+
  "</div>";

let min_input = document.getElementById("minInput");
let max_input = document.getElementById("maxInput");

let canvas = document.createElement("canvas");

let current_chart = undefined;

function money_round(num) {
  return Math.round(num * 1000) / 1000;
}

function clamp(min, max, val) {
  return Math.max(Math.min(max, val), min);
}

function extract_data(table) {
  let rows = table.getElementsByTagName("tr");
  order_prices = [];
  order_volumes = [];
  order_counts = [];

  for (let i = 1; i < rows.length - 1; ++i) {
    let row = rows[i];
    let cells = row.getElementsByTagName("td");
    order_prices.push(
      parseFloat(cells[0].innerText.replace(",", ".").replace(" ", ""))
    );
    order_volumes.push(parseFloat(cells[1].innerText.replace(" ", "")));
    order_counts.push(parseFloat(cells[3].innerText));
  }
  return {
    order_prices: order_prices,
    order_volumes: order_volumes,
    order_counts: order_counts,
  };
}

function to_dataset(orders) {
  let data = [];
  let volumes = orders.order_volumes;
  let prices = orders.order_prices;
  for (let i = 0; i < prices.length; ++i)
    data.push({ x: prices[i], y: volumes[i] });
  return data;
}

function to_accumulated(dataset, direction) {
  let acc = 0;
  let new_dataset = [];
  for (const point of dataset) {
    acc += point.y;
    new_dataset.push({ x: point.x, y: acc });
  }
  return new_dataset;
}

function get_instrument_name() {
  return document.getElementById("navi_link").children[2].innerText;
}

function get_last_actualization_text() {
  return document
    .getElementById("container_left")
    .getElementsByClassName("profile")[0]
    .getElementsByTagName("p")[2].innerText;
}

function createDatasets(buy_orders, sell_orders) {
  let buy_dataset = to_dataset(buy_orders);
  let sell_dataset = to_dataset(sell_orders);
  let buy_accumulated = to_accumulated(buy_dataset);
  let sell_accumulated = to_accumulated(sell_dataset);
  let datasets = [
    {
      type: "bar",
      label: "Buy volume",
      data: buy_dataset,
      backgroundColor: ["rgba(0, 100, 0, 1)"],
      borderColor: ["rgba(0, 100, 0, 1)"],
    },
    {
      label: "Sell volume",
      type: "bar",
      data: sell_dataset,
      backgroundColor: ["rgba(100, 0, 0, 0.6)"],
      borderColor: ["rgba(100, 0, 0, 0)"],
    },
    {
      type: "line",
      label: "Accumulated sell volume",
      data: sell_accumulated,
      backgroundColor: ["rgba(200, 0, 0, 0.2)"],
      stepped: "middle",
      fill: true,
      borderColor: ["rgba(200, 0, 0, 1)"],
      pointStyle: "dash",
      borderWidth: 1,
      pointBorderWidth: 0,
    },
    {
      type: "line",
      label: "Accumulated buy volume",
      data: buy_accumulated,
      backgroundColor: ["rgba(0, 200, 0, 0.2)"],
      stepped: "middle",
      fill: true,
      borderColor: ["rgba(0, 200, 0, 1)"],
      pointStyle: "dash",
      borderWidth: 1,
      pointBorderWidth: 0,
    },
  ];
  return datasets;
}

function draw_chart(datasets, min, max) {
  let ctx = document.getElementById("orderBookChart").getContext("2d");

  let options = {
    scales: {
      y: {
        beginAtZero: true,
        min: 0,
        title: {
          display: true,
          text: "Volume",
          font: {
            size: 12,
            weight: 'bold',
          },
        },
      },
      x: {
        type: "linear",
        min: min,
        max: max,
        title: {
          display: true,
          text: "Price",
          font: {
            size: 12,
            weight: 'bold',
          },
        },
      },
    },
    interaction: {
      mode: "nearest",
      axis: "x",
      intersect: false,
    },
    plugins: {
      tooltip: {
        callbacks: {
          beforeTitle: function (context) {
            return "Price: ";
          },
        },
      },
      title: {
        display: true,
        text: get_instrument_name(),
        font: {
          size: 22,
        },
      },
      subtitle: {
        display: true,
        text: get_last_actualization_text(),
        font: {
          size: 14,
        },
      },
    },
  };

  let orderBookChart = new Chart(ctx, {
    barPercentage: 1.0,
    categoryPercentage: 1.0,
    data: {
      datasets: [],
    },
    options: options,
  });
  return orderBookChart;
}

function setDatasetType(datasets, chart, type) {
  if (type === "accumulated") chart.data.datasets = [datasets[2], datasets[3]];
  else if (type === "orders") chart.data.datasets = [datasets[0], datasets[1]];
  else if (type === "accumulated_and_orders") chart.data.datasets = datasets;
  else return;
  chart.update();
}

function deactivateButton(button) {
  button.classList.remove("active");
}

function activateButton(button) {
  button.classList.add("active");
}

function createChangeChartTypeClickHandler(
  datasets,
  chart,
  targetButton,
  buttons,
  type
) {
  return (e) => {
    buttons.forEach(deactivateButton);
    activateButton(targetButton);
    setDatasetType(datasets, chart, type);
  };
}

window.onload = () => {
  let offset = 0.05;
  let buy_orders = extract_data(document.getElementById("arkusz_left"));
  let sell_orders = extract_data(document.getElementById("arkusz_right"));
  let buy_prices = buy_orders.order_prices;
  let sell_prices = sell_orders.order_prices;

  let start_min = money_round(buy_prices[0] * (1 - offset));
  let start_max = money_round(sell_prices[0] * (1 + offset));
  let min = 0;
  let max = sell_prices[sell_prices.length - 1];

  min_input.addEventListener("change", (e) => {
    e.target.value = clamp(min, max, e.target.value);
    order_book_chart.options.scales.x.min = money_round(e.target.value);
    order_book_chart.update();
  });

  max_input.addEventListener("change", (e) => {
    e.target.value = clamp(min, max, e.target.value);
    order_book_chart.options.scales.x.max = money_round(e.target.value);
    order_book_chart.update();
  });

  min_input.value = start_min;
  max_input.value = start_max;
  let datasets = createDatasets(buy_orders, sell_orders);
  order_book_chart = draw_chart(datasets, start_min, start_max);

  accumulated_button = document.getElementById("accumulatedButton");
  orders_button = document.getElementById("ordersButton");
  accumulated_and_orders_button = document.getElementById(
    "accumulatedAndOrdersButton"
  );
  let buttons = [
    accumulated_button,
    orders_button,
    accumulated_and_orders_button,
  ];

  orders_button.addEventListener(
    "click",
    createChangeChartTypeClickHandler(
      datasets,
      order_book_chart,
      orders_button,
      buttons,
      "orders"
    )
  );
  accumulated_button.addEventListener(
    "click",
    createChangeChartTypeClickHandler(
      datasets,
      order_book_chart,
      accumulated_button,
      buttons,
      "accumulated"
    )
  );
  accumulated_and_orders_button.addEventListener(
    "click",
    createChangeChartTypeClickHandler(
      datasets,
      order_book_chart,
      accumulated_and_orders_button,
      buttons,
      "accumulated_and_orders"
    )
  );
  accumulated_button.click();
};
